<?php

namespace appnic\SihfApi\Mappers;

use appnic\SihfApi\Helpers\GameTime;
use appnic\SihfApi\Resources\Goal;
use appnic\SihfApi\Resources\Penalty;
use appnic\SihfApi\Resources\Team;

class PenaltyMapper extends Mapper
{
    function initializeMappings(): void
    {
        $this->addDirectMapping('playerLicenceNr', 'playerId');
        $this->addDirectMapping('minutes');
        $this->addDirectMapping('id', 'code');
        $this->addMapping('time', function(Penalty $penalty, string $time) {
            $penalty->setStartTime(GameTime::toSeconds($time));
        });
        $this->addMapping('endTime', function(Penalty $penalty, string $time) {
            $penalty->setEndTime(GameTime::toSeconds($time));
        });
    }

    /**
     * @param array $source
     * @return Penalty
     */
    function map(array $source): \appnic\SihfApi\Resources\Resource
    {
        $penalty = new Penalty();
        return $this->performMapping($penalty, $source);
    }
}