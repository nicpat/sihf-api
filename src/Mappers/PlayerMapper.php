<?php

namespace appnic\SihfApi\Mappers;

use appnic\SihfApi\Resources\Player;

class PlayerMapper extends Mapper
{
    function initializeMappings(): void
    {
        $this->addDirectMapping('id');
        $this->addDirectMapping('licence', 'license');
        $this->addDirectMapping('jerseyNumber');
        $this->addDirectMapping('country');
        $this->addDirectMapping('teamId');
        $this->addDirectMapping('ageGroup');

        $this->addMapping('fullName', function(Player $resource, $value) {
            $nameArray = explode(' ', $value, 2);
            $resource->setLastName($nameArray[0]);
            $resource->setFirstName($nameArray[1]);
        });
    }

    /**
     * @param array $source
     * @return Player
     */
    function map(array $source): \appnic\SihfApi\Resources\Resource
    {
        $player = new Player();
        return $this->performMapping($player, $source);
    }
}