<?php

namespace appnic\SihfApi\Mappers;

use appnic\SihfApi\Collections\GameCollection;
use appnic\SihfApi\Resources\Game;
use appnic\SihfApi\Resources\Schedule;
use Carbon\Carbon;

class ScheduleMapper extends Mapper
{
    function initializeMappings(): void
    {
        $this->addDirectMapping('filters.0.selected','seasonYear');
        $this->addDirectMapping('filters.1.selected','teamId');
        $this->addMapping('data', function(Schedule $schedule, $value) {
            $games = new GameCollection();

            foreach($value as $gameEntry) {
                $result = [];
                foreach($gameEntry[6]['homeTeam'] as $value) {
                    $result[0][] = (int)$value;
                }

                foreach($gameEntry[6]['awayTeam'] as $value) {
                    $result[1][] = (int)$value;
                }

                $game = new Game();
                $game->setId($gameEntry[9]['gameId']);
                $game->setHomeTeam((new TeamMapper())->map($gameEntry[3]));
                $game->setAwayTeam((new TeamMapper())->map($gameEntry[4]));
                $game->setStartDateTime(
                    Carbon::createFromFormat('d.m.Y H:i', $gameEntry[1].' '.$gameEntry[2], 'Europe/Zurich'));
                $game->setResult($result);
                $games->append($game);
            }

            $schedule->setGames($games);
        });
    }

    /**
     * @param array $source
     * @return Schedule
     */
    function map(array $source): \appnic\SihfApi\Resources\Resource
    {
        $schedule = new Schedule();
        return $this->performMapping($schedule, $source);
    }
}