<?php

namespace appnic\SihfApi\Mappers;

use appnic\SihfApi\Resources\Team;

class TeamMapper extends Mapper
{
    function initializeMappings(): void
    {
        $this->addDirectMapping('id');
        $this->addDirectMapping('name');
        $this->addDirectMapping('acronym');
    }

    /**
     * @param array $source
     * @return Team
     */
    function map(array $source): \appnic\SihfApi\Resources\Resource
    {
        $team = new Team();
        return $this->performMapping($team, $source);
    }
}