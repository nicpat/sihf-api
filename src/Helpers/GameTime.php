<?php

namespace appnic\SihfApi\Helpers;

class GameTime
{
    /**
     * Format game time (e.g. 10:32) to seconds
     * @param string $time
     * @return int
     */
    public static function toSeconds(string $time) {
        $splittedValue = explode(':', $time);
        $minutes = (int)$splittedValue[0];
        $seconds = (int)$splittedValue[1];
        return (int)($minutes*60+$seconds);
    }

    /**
     * Format seconds into game time string
     *
     * @param int $seconds
     * @return string
     */
    public static function toGameTime(int $seconds) {
        return sprintf('%02d:%02d', intdiv($seconds, 60), $seconds % 60);
    }
}