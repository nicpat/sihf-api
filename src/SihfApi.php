<?php

namespace appnic\SihfApi;

use appnic\SihfApi\Exceptions\DataLoadException;
use appnic\SihfApi\Mappers\GameMapper;
use appnic\SihfApi\Mappers\ScheduleMapper;
use appnic\SihfApi\Resources\Game;
use appnic\SihfApi\Resources\Schedule;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class SihfApi
{
    const BASE_URL = 'https://data.sihf.ch/Statistic/api/cms/';

    /**
     * @var Client $client
     */
    private $client;

    public function __construct($client = null)
    {
        if($client == null) {
            $client = new Client([
                'base_uri' => self::BASE_URL,
                'timeout' => 20.0
            ]);
        }

        $this->client = $client;
    }

    /**
     * Returns the client
     *
     * @return Client
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * Loads the details of a game.
     *
     * @param int $id The official ID of the game.
     * @return Game
     * @throws DataLoadException Thrown when data could not be loaded (check inner exception for more details)
     */
    public function loadGame(int $id) : Game {
        try {
            $response = $this->client->request('GET', 'gameoverview?searchQuery='.$id);
        } catch (GuzzleException $exception) {
            throw new DataLoadException("Error while loading data from server.", $exception->getCode(), $exception);
        }

        $properties = json_decode((string)$response->getBody(), JSON_OBJECT_AS_ARRAY);
        return (new GameMapper())->map($properties);
    }

    /**
     * Loads the schedule of a given season.
     *
     * @param int|null $seasonYear The year of the season to request. This is always the year in which the playoffs
     *                              are played (Season 2018/2019 equals to $season = 2019).
     * @param int|null $teamId If the team ID is set, only return games from this team
     * @return Schedule
     * @throws DataLoadException Thrown when data could not be loaded (check inner exception for more details)
     */
    public function loadSchedule(int $seasonYear = null, int $teamId = null) : Schedule {
        $url = 'table?alias=results&searchQuery=1,2,10,11//1,81,90&&filterBy=Season&filterQuery=';

        /*
         * If the season has not been set, request the current season. If the current month is August, the upcoming
         * season is returned.
         */
        if($seasonYear == null) {
            $today = Carbon::now();
            if($today->month < 8) {
                $url .= $today->year;
            } else {
                $url .= $today->year+1;
            }
        } else {
            $url .= $seasonYear;
        }

        // Filter for the team ID
        if($teamId != null) {
            $url .= '/'.$teamId;
        }

        try {
            $response = $this->client->request('GET', $url);
        } catch (GuzzleException $exception) {
            throw new DataLoadException("Error while loading data from server.", $exception->getCode(), $exception);
        }

        $properties = json_decode((string)$response->getBody(), JSON_OBJECT_AS_ARRAY);
        return (new ScheduleMapper())->map($properties);
    }
}