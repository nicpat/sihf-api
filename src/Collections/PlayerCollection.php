<?php

namespace appnic\SihfApi\Collections;

use appnic\SihfApi\Resources\Player;

class PlayerCollection extends Collection
{
    public $acceptsType = "appnic\SihfApi\Resources\Player";

    public function __construct(Player ...$players)
    {
        parent::__construct($players);
    }
}