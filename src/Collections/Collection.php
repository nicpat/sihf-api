<?php

namespace appnic\SihfApi\Collections;

use ArrayObject;

class Collection extends ArrayObject
{
    public $acceptsType = "Object";

    public function offsetSet($index, $newval)
    {
        if (!($newval instanceof $this->acceptsType)) {
            throw new \InvalidArgumentException("Value must be of type " . $this->acceptsType);
        }

        parent::offsetSet($index, $newval);
    }

    /**
     * Appends an array of elements to the collection
     * @param array $values
     */
    public function appendMany(array $values)
    {
        foreach($values as $value) {
            $this->append($value);
        }
    }
}