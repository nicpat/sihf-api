<?php

namespace appnic\SihfApi\Collections;

use appnic\SihfApi\Resources\Penalty;

class PenaltyCollection extends Collection
{
    public $acceptsType = "appnic\SihfApi\Resources\Penalty";

    public function __construct(Penalty ...$penalties)
    {
        parent::__construct($penalties);
    }
}