<?php

namespace appnic\SihfApi\Collections;

use appnic\SihfApi\Resources\Goal;

class GoalCollection extends Collection
{
    public $acceptsType = "appnic\SihfApi\Resources\Goal";

    public function __construct(Goal ...$goals)
    {
        parent::__construct($goals);
    }
}