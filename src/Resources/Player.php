<?php

namespace appnic\SihfApi\Resources;

class Player extends Resource
{
    /**
     * @var int $id
     */
    private $id;

    /**
     * @var Team $team
     */
    private $team;

    /**
     * @var string $firstName
     */
    private $firstName;

    /**
     * @var string $lastName
     */
    private $lastName;

    /**
     * @var int $jerseyNumber
     */
    private $jerseyNumber;

    /**
     * @var string $country
     */
    private $country;

    /**
     * @var string $license
     */
    private $license;

    /**
     * @var int $ageGroup
     */
    private $ageGroup;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Team
     */
    public function getTeam(): Team
    {
        return $this->team;
    }

    /**
     * @param Team $team
     */
    public function setTeam(Team $team): void
    {
        $this->team = $team;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return int
     */
    public function getJerseyNumber(): int
    {
        return $this->jerseyNumber;
    }

    /**
     * @param int $jerseyNumber
     */
    public function setJerseyNumber(int $jerseyNumber): void
    {
        $this->jerseyNumber = $jerseyNumber;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getLicense(): string
    {
        return $this->license;
    }

    /**
     * @param string $license
     */
    public function setLicense(string $license): void
    {
        $this->license = $license;
    }

    /**
     * @return int
     */
    public function getAgeGroup(): int
    {
        return $this->ageGroup;
    }

    /**
     * @param int $ageGroup
     */
    public function setAgeGroup(int $ageGroup): void
    {
        $this->ageGroup = $ageGroup;
    }
}