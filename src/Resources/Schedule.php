<?php

namespace appnic\SihfApi\Resources;

use appnic\SihfApi\Collections\GameCollection;

class Schedule extends Resource
{
    /**
     * Get the year of this schedule
     * @var int $seasonYear
     */
    private $seasonYear;

    /**
     * The ID of the team this schedule is of
     * @var int $teamId
     */
    private $teamId;

    /**
     * List of games in this schedule
     * @var GameCollection $games
     */
    private $games;

    /**
     * @return int
     */
    public function getSeasonYear(): int
    {
        return $this->seasonYear;
    }

    /**
     * @param int $seasonYear
     */
    public function setSeasonYear(int $seasonYear): void
    {
        $this->seasonYear = $seasonYear;
    }

    /**
     * @return int
     */
    public function getTeamId(): int
    {
        return $this->teamId;
    }

    /**
     * @param int $teamId
     */
    public function setTeamId(int $teamId): void
    {
        $this->teamId = $teamId;
    }

    /**
     * @return GameCollection
     */
    public function getGames(): GameCollection
    {
        return $this->games;
    }

    /**
     * @param GameCollection $games
     */
    public function setGames(GameCollection $games): void
    {
        $this->games = $games;
    }
}