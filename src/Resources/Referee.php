<?php

namespace appnic\SihfApi\Resources;

/**
 * Class Referee
 * @package appnic\SihfApi\Resources
 */
class Referee extends Resource
{
    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var int
     */
    private $jerseyNumber;

    /**
     * @var string
     */
    private $country;

    /**
     * @var bool
     */
    private $isLinesman;

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return int
     */
    public function getJerseyNumber(): int
    {
        return $this->jerseyNumber;
    }

    /**
     * @param int $jerseyNumber
     */
    public function setJerseyNumber(int $jerseyNumber): void
    {
        $this->jerseyNumber = $jerseyNumber;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return bool
     */
    public function isLinesman(): bool
    {
        return $this->isLinesman;
    }

    /**
     * @param bool $isLinesman
     */
    public function setIsLinesman(bool $isLinesman): void
    {
        $this->isLinesman = $isLinesman;
    }
}