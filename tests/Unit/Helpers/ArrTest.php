<?php

namespace appnic\SihfApi\tests\Unit\Helpers;

use appnic\SihfApi\Helpers\Arr;
use PHPUnit\Framework\TestCase;

class ArrTest extends TestCase
{
    /**
     * @param array $testArray
     * @dataProvider arrayProvider
     * @covers       \appnic\SihfApi\Helpers\Arr::dot
     */
    public function testDot($testArray)
    {
        $dotArray = Arr::dot($testArray);

        $this->assertSame('loremvalue', $dotArray['lorem']);
        $this->assertSame('dolorvalue', $dotArray['ipsum.dolor']);
        $this->assertSame('first', $dotArray['ipsum.sit.0']);
        $this->assertSame('second', $dotArray['ipsum.sit.1']);
        $this->assertSame('third', $dotArray['ipsum.sit.2']);
        $this->assertSame('conseteturvalue', $dotArray['ipsum.amet.consetetur']);
        $this->assertSame('sadipscingvalue', $dotArray['ipsum.amet.sadipscing']);
    }

    /**
     * @param array $testArray
     * @dataProvider arrayProvider
     * @covers \appnic\SihfApi\Helpers\Arr::dotSearch
     */
    public function testDotSearch($testArray) {
        $this->assertSame('loremvalue', Arr::dotSearch('lorem', $testArray));
        $this->assertSame('conseteturvalue', Arr::dotSearch('ipsum.amet.consetetur', $testArray));
        $this->assertTrue(is_array(Arr::dotSearch('ipsum.sit', $testArray)));
        $this->assertTrue(is_array(Arr::dotSearch('ipsum.amet', $testArray)));
        $this->assertNull(Arr::dotSearch('doesnt.exist', $testArray));
        $this->assertNull(Arr::dotSearch('ipsum.dolor.doesntexist', $testArray));
    }

    public function arrayProvider()
    {
        return [
            [
                'Default Array' => [
                    'lorem' => 'loremvalue',
                    'ipsum' => [
                        'dolor' => 'dolorvalue',
                        'sit' => [
                            'first',
                            'second',
                            'third',
                        ],
                        'amet' => [
                            'consetetur' => 'conseteturvalue',
                            'sadipscing' => 'sadipscingvalue'
                        ]
                    ]
                ]
            ]
        ];
    }
}
