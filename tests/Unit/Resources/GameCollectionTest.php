<?php

namespace appnic\SihfApi\tests\Unit\Resources;

use appnic\SihfApi\Collections\GameCollection;
use appnic\SihfApi\Resources\Game;
use appnic\SihfApi\Resources\Team;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class GameCollectionTest extends TestCase
{
    protected $games;

    public function __construct()
    {
        parent::__construct();
        $this->games = $this->createGameArray();
    }

    public function testCreate()
    {
        $collection = new GameCollection();
        $collection->appendMany($this->games);
        $this->assertSame(count($this->games), count($collection));

        return $collection;
    }

    /**
     * @depends testCreate
     * @param GameCollection $collection
     */
    public function testGet(GameCollection $collection) {
        $this->assertNotNull($collection[0]);
        $this->assertNotNull($collection[count($collection)-1]);
        $this->assertEquals($this->games[0], $collection[0]);
        $this->assertEquals($this->games[count($this->games)-1], $collection[count($collection)-1]);
    }

    public function createGameArray() : array {
        $gameArray = [
            [ 1, 102127, "SCL Tigers", 102323, "Guest Team", 5, 1 ]
        ];

        $return = [];

        foreach($gameArray as $rawGame) {
            $game = new Game();
            $game->setId($rawGame[0]);

            $teamHome = new Team();
            $teamHome->setId($rawGame[1]);
            $teamHome->setName($rawGame[2]);
            $game->setHomeTeam($teamHome);

            $teamAway = new Team();
            $teamAway->setId($rawGame[3]);
            $teamAway->setName($rawGame[4]);
            $game->setAwayTeam($teamAway);

            $game->setResult([$rawGame[5], $rawGame[6]]);
            $return[] = $game;
        }

        return $return;
    }
}
