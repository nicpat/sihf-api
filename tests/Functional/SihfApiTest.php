<?php

namespace appnic\SihfApi\tests\Functional;

use appnic\SihfApi\Exceptions\DataLoadException;
use appnic\SihfApi\SihfApi;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class SihfApiTest extends TestCase
{
    /**
     * @covers \appnic\SihfApi\SihfApi
     * @return SihfApi
     */
    public function testCreate()
    {
        $sihfApi = new SihfApi();

        $this->assertNotNull($sihfApi->getClient());
        $this->assertSame(SihfApi::BASE_URL, (string)$sihfApi->getClient()->getConfig('base_uri'));

        return $sihfApi;
    }

    /**
     * @covers \appnic\SihfApi\SihfApi
     */
    public function testCreateCustomClient()
    {
        $base_uri = 'https://example.org/test/';

        $client = new Client([
            'base_uri' => $base_uri
        ]);
        $sihfApi = new SihfApi($client);

        $this->assertNotNull($sihfApi->getClient());
        $this->assertSame($base_uri, (string)$sihfApi->getClient()->getConfig('base_uri'));
    }

    /**
     * @dataProvider scheduleClientProvider
     * @param MockHandler $mock
     * @throws DataLoadException
     */
    public function testLoadSchedule(MockHandler $mock)
    {
        $sihfApi = new SihfApi(new Client(['handler' => HandlerStack::create($mock)]));

        if($this->dataDescription() == 'server_error') {
            $this->expectException('appnic\SihfApi\Exceptions\DataLoadException');
        }
        $schedule = $sihfApi->loadSchedule();
        $this->assertInstanceOf('\appnic\SihfApi\Resources\Schedule', $schedule);
    }

    /**
     * @dataProvider scheduleClientProvider
     * @param MockHandler $mock
     * @throws DataLoadException
     */
    public function testLoadGame(MockHandler $mock)
    {
        $sihfApi = new SihfApi(new Client(['handler' => HandlerStack::create($mock)]));

        if($this->dataDescription() == 'server_error') {
            $this->expectException('appnic\SihfApi\Exceptions\DataLoadException');
        }
        $game = $sihfApi->loadGame(12345678);
        $this->assertInstanceOf('\appnic\SihfApi\Resources\Game', $game);
    }

    public function scheduleClientProvider() {
        $scheduleJson = file_get_contents(__DIR__.'/Json/schedule.json');
        return [
            'sucessful_response' => [new MockHandler([new Response(200, [], $scheduleJson)])],
            'server_error' => [new MockHandler([new Response(503, [], "Server error.")])],
        ];
    }

    public function gamedetailClientProvider() {
        $gamedetailJson = file_get_contents(__DIR__.'/Json/gamedetail.json');
        return [
            'sucessful_response' => [new MockHandler([new Response(200, [], $gamedetailJson)])],
            'server_error' => [new MockHandler([new Response(503, [], "Server error.")])],
        ];
    }
}
